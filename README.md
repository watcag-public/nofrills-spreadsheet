# nofrills-spreadsheet

### authors: shounak datta, tim vela (spring 2020), julian parkin (spring 2021)

## Pre-requisites (already installed in your Docker containers, so skip this step)
```bash
$ apt-get install python3-tk
$ pip3 install tkinter pandas
```

## Usage
To use this script, after cloning the repo, just run the following command where you replace `my_csv.csv` with the one you want to edit.

```bash
$ python3 nofrills-spreadsheet.py my_csv.csv
```

Start with a boring CSV
![](boring.png)

Make some edits, make it delicious
![](delicious.png)

Check the CSV format on the terminal
```bash
$ cat my_csv.csv
Cycle,Multiplier1_Op1,Multiplier1_Op2
0,DUCKLING,BIRYANI
1,SQUIRREL,BIRYANI
2,MASALA,PARROT
3,BEEF,CUPCAKE
```

