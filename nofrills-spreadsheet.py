#!/usr/bin/env python3

import sys
import pandas as pd

from tkinter import *

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print(f'Usage: {sys.argv[0]} <csv file>')
        exit(1)

    try:
        data = pd.read_csv(sys.argv[1], dtype=str, keep_default_na=False)
    except FileNotFoundError:
        print(f'Unable to find file: {sys.argv[1]}')
        exit(1)

    master = Tk()
    master.title("No-Frills Spreadsheet")

    spreadsheet = Frame(master)
    spreadsheet.grid(row=0, column=0, padx=4, pady=4)

    columns = data.columns.values.tolist()

    for cindex, col in enumerate(columns):
        label = Label(spreadsheet, text=col, font=10)
        label.grid(row=0, column=cindex)

    grid = []

    for rindex, row in enumerate(data[columns[0]]):
        new_row = []
        label = Label(spreadsheet, text=row, font=10)
        label.grid(row=rindex+1)
        for cindex, col in enumerate(columns[1:]):
            entry_box = Entry(spreadsheet)
            entry_box.insert(END, data.at[rindex, columns[cindex+1]]); 
            entry_box.grid(row=rindex+1, column=cindex+1)
            new_row.append(entry_box)
        grid.append(new_row)

    buttons = Frame(master)
    buttons.grid(row=1, column=0, padx=4, pady=4)

    def makeCsv():
        for row_index, row in enumerate(grid):
            if len(row) > 0:
                for cindex, col in enumerate(row):
                    data.at[row_index, columns[cindex+1]] = row[cindex].get()
        data.to_csv(sys.argv[1], index=False)

    save_button = Button(buttons, text="Save", command=makeCsv)
    save_button.grid(row=0, column=0)

    master.mainloop()
